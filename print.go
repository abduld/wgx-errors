package errors

import (
	"bytes"
	"encoding/json"
	"fmt"

	goerr "github.com/go-errors/errors"
	"gitlab.com/abduld/wgx-api/models"
)

// Returns zero values if e has no stacktrace
func Location(e error) (file string, line int) {
	s := Stack(e)
	if len(s) > 0 {
		sf := goerr.NewStackFrame(s[0])
		return sf.File, sf.LineNumber
	}
	return "", 0
}

// SourceLine returns the string representation of
// Location's result or an empty string if there's
// no stracktrace.
func SourceLine(e error) string {
	file, line := Location(e)
	if line != 0 {
		return fmt.Sprintf("%s:%d", file, line)
	}
	return ""
}

// Returns the error's stacktrace as a string formatted
// the same way as golangs runtime package.
// If e has no stacktrace, returns an empty string.
func Stacktrace(e error) string {
	s := Stack(e)
	if len(s) > 0 {
		buf := bytes.Buffer{}
		for _, fp := range s {
			sf := goerr.NewStackFrame(fp)
			buf.WriteString(sf.String())
		}
		return buf.String()
	}
	return ""
}

// Returns e.Error() and e's stacktrace and user message, if set.
func Details(e error) string {
	if e == nil {
		return ""
	}
	msg := e.Error()
	userMsg := UserMessage(e)
	if userMsg != "" {
		msg = fmt.Sprintf("%s\n\nUser Message: %s", msg, userMsg)
	}
	s := Stacktrace(e)
	if s != "" {
		msg += "\n\n" + s
	}
	return msg
}

// Returns e.Error() and e's stacktrace and user message, if set.
func (e *Err) Marshal() ([]byte, error) {
	type _trace struct {
		Package        string  `json:"package"`
		File           string  `json:"file"`
		LineNumber     int     `json:"line_number"`
		ProgramCounter uintptr `json:"program_counter"`
		Function       string  `json:"function"`
	}
	type _result struct {
		Message     string   `json:"message"`
		UserMessage string   `json:"user_message"`
		StackTrace  []_trace `json:"stack_trace"`
	}

	if e == nil {
		return []byte{}, nil
	}
	msg := e.Error()
	userMsg := UserMessage(e)

	trace := []_trace{}
	for _, fp := range Stack(e) {
		sf := goerr.NewStackFrame(fp)
		trace = append(trace, _trace{
			Package:        sf.Package,
			File:           sf.File,
			LineNumber:     sf.LineNumber,
			ProgramCounter: sf.ProgramCounter,
			Function:       sf.Name,
		})
	}

	return json.Marshal(_result{
		Message:     msg,
		UserMessage: userMsg,
		StackTrace:  trace,
	})
}

func Marshal(err error) ([]byte, error) {
	switch err := err.(type) {
	case *Err:
		return err.Marshal()
	default:
		type _result struct {
			Error string `json:"error"`
		}
		return json.Marshal(_result{
			Error: Details(err),
		})
	}
}

func ToModel(err error) *models.Error {
	if err == nil {
		return &models.Error{
			Code:    200,
			Message: "Success",
		}
	}

	msg := err.Error()
	userMsg := UserMessage(err)

	trace := []*models.ErrorStackTrace{}
	for _, fp := range Stack(err) {
		sf := goerr.NewStackFrame(fp)
		trace = append(trace, &models.ErrorStackTrace{
			Package:        sf.Package,
			File:           sf.File,
			LineNumber:     int64(sf.LineNumber),
			ProgramCounter: int64(sf.ProgramCounter),
			Function:       sf.Name,
		})
	}

	return &models.Error{
		Code:        404,
		Message:     msg,
		UserMessage: userMsg,
		StackTrace:  trace,
	}
}
